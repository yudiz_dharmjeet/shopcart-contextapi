import React from "react";
import { CartState } from "../../Context/Context";

import "./Filters.scss";

function Filters() {
  const {
    productState: { sort, byStock, byFastDelivery },
    productDispatch,
  } = CartState();

  return (
    <div className="filters">
      <span className="title">Filter Products</span>
      <span>
        <input
          type="radio"
          id="accending"
          name="order"
          value="accending"
          onChange={() =>
            productDispatch({
              type: "SORT_BY_PRICE",
              payload: "lowToHigh",
            })
          }
          checked={sort === "lowToHigh" ? true : false}
        />
        <label htmlFor="accending">Accending</label>
      </span>
      <span>
        <input
          type="radio"
          id="decending"
          name="order"
          value="decending"
          onChange={() =>
            productDispatch({
              type: "SORT_BY_PRICE",
              payload: "highToLow",
            })
          }
          checked={sort === "highToLow" ? true : false}
        />
        <label htmlFor="decending">Decending</label>
      </span>
      <span>
        <input
          type="checkbox"
          id="outofstock"
          name="outofstock"
          checked={byStock}
          onChange={() =>
            productDispatch({
              type: "FILTER_BY_STOCK",
            })
          }
        />
        <label htmlFor="outofstock">Include Out of Stock</label>
      </span>
      <span>
        <input
          type="checkbox"
          id="fastdelivery"
          name="fastdelivery"
          checked={byFastDelivery}
          onChange={() =>
            productDispatch({
              type: "FILTER_BY_DELIVERY",
            })
          }
        />
        <label htmlFor="fastdelivery">Fast Delivery Only</label>
      </span>
      <button
        onClick={() =>
          productDispatch({
            type: "CLEAR_FILTERS",
          })
        }
      >
        Clear Filters
      </button>
    </div>
  );
}

export default Filters;
