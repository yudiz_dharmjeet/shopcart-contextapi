import React from "react";
import { Link } from "react-router-dom";

import CartImg from "../../Assets/Images/cart-white.svg";
import { CartState } from "../../Context/Context";

import "./Header.scss";

function Header() {
  const {
    state: { cart },
    productDispatch,
    deleteItem,
  } = CartState();

  return (
    <header className="header">
      <div className="container">
        <h3>
          <Link className="link" to="/">
            ShopCart
          </Link>
        </h3>
        <input
          className="header__search-input"
          type="text"
          name="search"
          id="search"
          placeholder="Search"
          onChange={(e) =>
            productDispatch({
              type: "FILTER_BY_SEARCH",
              payload: e.target.value,
            })
          }
        />
        {/* <div className="header__cart-icon">Cart</div> */}
        <div className="dropdown">
          <button className="dropbtn">
            <Link to="/cart" className="dropbtn__inner">
              <img src={CartImg} alt="CartImg" />
            </Link>
            <p>{cart.length}</p>
          </button>
          <div className="dropdown-content">
            {cart.length > 0 ? (
              <>
                {cart.map((prod) => (
                  <span key={prod.id} className="cart_item">
                    <img
                      src={prod.image}
                      alt={prod.name}
                      className="cart_item_img"
                    />
                    <div className="cart_item_details">
                      <span>{prod.name}</span>
                      <span>₹ {prod.price.split(".")[0]}</span>
                    </div>
                    <span
                      className="cart_remove_btn"
                      onClick={() => deleteItem(prod)}
                    >
                      X
                    </span>
                  </span>
                ))}
                <Link to="/cart">
                  <button className="gotocart_btn">Go To Cart</button>
                </Link>
              </>
            ) : (
              <span className="cart_item">Cart is Empty!</span>
            )}
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
