import React from "react";
import PropTypes from "prop-types";

import "./SingleProduct.scss";
import { CartState } from "../../Context/Context";

function SingleProduct({ prod }) {
  const {
    state: { cart },
    dispatch,
    deleteItem,
  } = CartState();

  console.log(cart);

  return (
    <div className="product">
      <div className="product__card">
        <img src={prod.image} alt={prod.name} />
        <div className="product__card-body">
          <h3>{prod.name}</h3>
          <div className="product__card-middle">
            <span>₹ {prod.price.split(".")[0]}</span>
            {prod.fastDelivery ? (
              <div>Fast Delivery</div>
            ) : (
              <div>4 days delivery</div>
            )}
          </div>

          {cart.some((item) => item.id === prod.id) ? (
            <button
              className="product__card-remove-btn"
              onClick={() => deleteItem(prod)}
            >
              Remove from Cart
            </button>
          ) : !prod.inStock ? (
            <button className="product__card-outstock-btn">Out of Stock</button>
          ) : (
            <button
              className="product__card-add-btn"
              onClick={() => {
                dispatch({ type: "ADD_TO_CART", payload: prod });
              }}
            >
              Add to Cart
            </button>
          )}
        </div>
      </div>
    </div>
  );
}

SingleProduct.propTypes = {
  prod: PropTypes.object,
};

export default SingleProduct;
