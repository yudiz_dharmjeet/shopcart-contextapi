import React from "react";
import { Filters, SingleProduct } from "../../Components";
import { CartState } from "../../Context/Context";

import "./Home.scss";

function Home() {
  const {
    state: { products },
    productState: { sort, byStock, byFastDelivery, searchQuery },
  } = CartState();

  const transformProducts = () => {
    let sortedProducts = products;

    if (sort) {
      sortedProducts = sortedProducts.sort((a, b) =>
        sort === "lowToHigh" ? a.price - b.price : b.price - a.price
      );
    }

    if (!byStock) {
      sortedProducts = sortedProducts.filter((prod) => prod.inStock);
    }

    if (byFastDelivery) {
      sortedProducts = sortedProducts.filter((prod) => prod.fastDelivery);
    }

    if (searchQuery) {
      sortedProducts = sortedProducts.filter((prod) =>
        prod.name.toLowerCase().includes(searchQuery)
      );
    }
    console.log(sortedProducts);
    return sortedProducts;
  };

  return (
    <div className="home">
      <div className="home__filter-section">
        <Filters />
      </div>
      <div className="home__products-section">
        {transformProducts().map((prod) => {
          return <SingleProduct key={prod.id} prod={prod} />;
        })}
      </div>
    </div>
  );
}

export default Home;
