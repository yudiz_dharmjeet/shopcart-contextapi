import React, { useEffect, useState } from "react";
import { CartState } from "../../Context/Context";

import "./Cart.scss";

function Cart() {
  const {
    state: { cart },
    dispatch,
    deleteItem,
  } = CartState();

  const [total, setTotal] = useState();

  useEffect(() => {
    setTotal(
      cart.reduce((acc, curr) => {
        return acc + parseInt(curr.price) * curr.qty;
      }, 0)
    );
  }, [cart]);

  return (
    <div className="cartpage">
      <div className="cartpage__product-section">
        {cart.map((item) => {
          return (
            <div className="cartpage__card" key={item.id}>
              <img
                src={item.image}
                style={{
                  width: "70px",
                  height: "70px",
                  objectFit: "cover",
                  borderRadius: "50%",
                  marginRight: "18px",
                }}
              />
              <h4>{item.name}</h4>
              <div
                style={{
                  flex: 2,
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <p>₹ {item.price}</p>
                <div>
                  <label htmlFor="quantity">Quantity : </label>
                  <select
                    id="quantity"
                    value={item.qty}
                    onChange={(e) =>
                      dispatch({
                        type: "CHANGE_CART_QTY",
                        payload: {
                          id: item.id,
                          qty: e.target.value,
                        },
                      })
                    }
                  >
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                  </select>
                </div>
                <p
                  style={{ cursor: "pointer" }}
                  onClick={() => deleteItem(item)}
                >
                  X
                </p>
              </div>
            </div>
          );
        })}
      </div>
      <div className="cartpage__total-section summary">
        <span className="title">Subtotal ({cart.length}) items</span>
        <span style={{ fontWeight: 700, fontSize: 20 }}>Total: ₹ {total}</span>
        <button disabled={cart.length === 0}>Proceed to Checkout</button>
      </div>
    </div>
  );
}

export default Cart;
